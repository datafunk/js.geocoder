/***************!
    JS Geocoder
    -----------
    Author:     pborbely@fairfaxmedia.com.au
    Born:       August 2014
    Version:    0.3

    Notes, Dependencies, Credits
    -----------
    jQuery 1.11.2               http://code.jquery.com/jquery-1.11.2.min.js
    Papaparse:                  http://papaparse.com/
    Google Maps API Geocoder:   https://maps.googleapis.com/maps/api/js
    HTML5 File API:             http://dev.w3.org/2009/dap/file-system/file-dir-sys.html
    File API support:           https://maps.googleapis.com/maps/api/js
    Download function:          http://danml.com/js/download.js
    -----------
    (C) 2015 Peter Borbely
    http://opensource.org/licenses/GPL-3.0

*****************/

var BP = BP || {};
BP.geocoder = (function() {


    var version = 0.04;
    var data_in = [];
    var data_out = [];
    var data_str = 'ID, Addr, ReturnedAddr, Suburb, Postcode, Lat, Lng, Aussie, Status \n'; // first row as header, appended rows as data
    var i = 1;
    var j = 0;
    var err = 0;
    var file2parse = $('input[type=file]');
    var fileName = '';
    var latLng = '';
    var title = document.title;
    var frequency = 1200;



    $('#tc_accepted')[0].addEventListener('change', checkTC, false);
    $('#submit')[0].addEventListener('click', parseData, false);
    // $('#frequency')[0].textContent = frequency / 1000;
    $('#version')[0].textContent = version;

    function checkTC() {
        if ($('#tc_accepted')[0].checked) {
            $('#files')[0].removeAttribute('disabled');
            $('#files')[0].addEventListener('change', handleFileSelect, false);
            // $('#submit')[0].style.visibility = 'visible';
            // $('#progress')[0].style.visibility = 'visible';
        } else {
            $('#files')[0].setAttribute('disabled', 'disabled');
            $('#submit')[0].style.visibility = 'hidden';
            $('#progress')[0].style.visibility = 'hidden';
            // unchecking TC removes selected file
            var input = $('#files');
            input.replaceWith(input.val('').clone(true));
            $('#list').html('');
        }
    }

    (function toggleModals() {
        $('.link').click(function(event) {
            event.preventDefault();
            // console.log(event.target.getAttribute('href'));
            var modal = event.target.getAttribute('href');
            $('.modal').fadeOut(400, function() {
                setTimeout(function() {
                    $('' + modal).fadeIn(400);
                }, 500);
            });
        });
    })();



    function countResults() {
        j = j + 1;
        // console.log(i,j);
        progress();


        if (j === i - 1) {
            // console.log('now time to move on');
            $('#counter')[0].textContent = j;
            $('#submit')[0].textContent = 'Done!';
            stringifyProcessedData();
        } else {
            // console.log('there\'s more to do');
            $('#counter')[0].textContent = j;
        }

    }


    function progress() {
        // console.log(i,j, data_in.length);
        $('#total')[0].textContent = data_in.length;

        var pct = parseInt((100 / data_in.length) * j);
        var t = '[' + pct + '%] ' + title;
        document.title = t;

        $('#pct')[0].textContent = pct;
        $('#errors')[0].textContent = err;
        $('#p_bar').animate({
                width: pct + '%'
            },
            400,
            function() {
                /* stuff to do after animation is complete */

            });
    }

    function handleFileSelect(evt) {

            $('#submit')[0].style.visibility = 'visible';
            $('#progress')[0].style.visibility = 'visible';

            var files = evt.target.files; // FileList object
            fileName = files[0].name;

            // files is a FileList of File objects. List some properties.
            var output = [];
            for (var i = 0, f; f = files[i]; i++) {
                output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
                    f.size, ' bytes, last modified: ',
                    f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
                    '</li>');
            }
            $('#list').html('<ul>' + output.join('') + '</ul>');

        }
        // END handleFileSelect


    function parseData() {
            console.log(' == parseData called ==');

            $('#submit')[0].textContent = 'Working...';

            file2parse.parse({
                config: {
                    // default config options
                    delimiter: "",
                    // header field names wil be returned in meta object
                    header: true,
                    // dynamicTyping converts numeric and boolean data to their respective types
                    dynamicTyping: true,
                    // preview limits the number of rows to be parsed if > 0
                    preview: 0,
                    // To stream the input, define a callback function to receive results row-by-row rather than together at the end
                    // or undefined
                    step: function(results, parser) {
                        // console.log(results, parser);
                        // console.log(["Row data:", results.data]);
                        // console.error(["Row errors:", results.errors]);
                        data_in.push(results.data[0]);
                        // writeSrc2table(results.data[0], i);
                        // console.log(results.data[0].Addr);
                        if (results.data[0].Addr == '' || results.data[0].Addr == undefined) {
                            results.data[0].fAddr = 'N/A';
                            results.data[0].Lat = 0;
                            results.data[0].Lng = 0;
                            results.data[0].Aussie = 'zero_input';
                            results.data[0].status = 'zero_input';
                            err = err + 1;
                            countResults();
                        } else {
                            codeAddress(results.data[0], i);
                        }

                        i = i + 1;
                        // progress();

                    },
                    // step: undefined,
                    encoding: "",
                    worker: true,
                    comments: false,
                    complete: undefined,
                    download: false,
                    keepEmptyRows: false,
                    chunk: undefined
                },
                before: function(file, inputElem, myRawData) {
                    // executed before parsing each file begins;
                    // what you return here controls the flow
                    // console.log('executing BEFORE the first parse action');
                },
                error: function(err, file, inputElem, reason) {
                    // executed if an error occurs while loading the file,
                    // or if before callback aborted for some reason
                    console.error(err, file, inputElem, reason);
                },
                complete: function(results, file) {
                    // executed after all files are complete
                    // When streaming, parse results are not available in this callback.
                    // console.log("Parsing complete:", results, file);
                }
            });

        }
        // END parseData



    function codeAddress(data_obj, i) {

            var addr = data_obj.Addr;
            // console.log(addr, i);

            var geocoder = new google.maps.Geocoder();
            setTimeout(function() {

                geocoder.geocode({
                    'address': addr
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        console.log([results[0], data_obj]);

                        latLng = results[0].geometry.location.toString();
                        // console.log(latLng);
                        latLng = latLng.replace(/\(/, '');
                        latLng = latLng.replace(/\)/, '');
                        // latLng = latLng.replace(/\s)/,'');
                        latLng = latLng.split(",");
                        // console.log(latLng);

                        // data_obj.suburb = results[0].address_components[2].long_name;
                        for (var i in results[0].address_components) {
                            if (results[0].address_components[i].types[0] === 'locality') {
                                data_obj.suburb = results[0].address_components[i].long_name;
                                break;
                            }
                        }



                        // data_obj.postcode = results[0].address_components[5].short_name;
                        for (var i in results[0].address_components) {
                            if (results[0].address_components[i].types[0] === 'postal_code') {
                                data_obj.postcode = results[0].address_components[i].long_name;
                                break;
                            }
                        }


                        data_obj.fAddr = results[0].formatted_address;
                        data_obj.Lat = latLng[0];
                        data_obj.Lng = latLng[1];
                        data_obj.status = status;

                        if (results[0].formatted_address.indexOf('Australia') == -1) {
                            data_obj.Aussie = 0;
                        } else {
                            data_obj.Aussie = 1;
                        }

                        countResults();


                    } else {
                        // alert("Geocode was not successful for the following reason: " + status);
                        console.error(status.toString());
                        data_obj.fAddr = 'N/A';
                        data_obj.Lat = 0;
                        data_obj.Lng = 0;
                        data_obj.suburb = 'N/A';
                        data_obj.postcode = 0;
                        data_obj.Aussie = 'no_result';
                        data_obj.status = status.toString();
                        err = err + 1;
                        countResults();
                    }
                    // console.log('geocoder called');

                });
            }, i * frequency);

        }
        // END codeAddress

    function stringifyProcessedData() {
        // console.log('stringify called');
        // console.log(data_in);

        for (var k in data_in) {
            // formAddress = formAddress.replace(/\,/g, ' | ');
            data_out.push(data_in[k].ID + ',"' + data_in[k].Addr + '","' + data_in[k].fAddr + '",' + data_in[k].suburb + ',' + data_in[k].postcode + ',' + data_in[k].Lat + ',' + data_in[k].Lng + ',"' + data_in[k].Aussie + '","' + data_in[k].status + '"');
        }

        for (var l in data_out) {
            data_str = data_str + data_out[l] + '\n';
        }

        download(data_str, 'geocoded_' + fileName, 'text/plain');

    }

    function download(strData, strFileName, strMimeType) {
        var D = document,
            a = D.createElement("a");
        strMimeType = strMimeType || "application/octet-stream";

        if (navigator.msSaveBlob) { // IE10+
            return navigator.msSaveBlob(new Blob([strData], {
                type: strMimeType
            }), strFileName);
        } /* end if(navigator.msSaveBlob) */


        if ('download' in a) { //html5 A[download]
            if (window.URL) {
                a.href = window.URL.createObjectURL(new Blob([strData]));

            } else {
                a.href = "data:" + strMimeType + "," + encodeURIComponent(strData);
            }
            a.setAttribute("download", strFileName);
            a.innerHTML = "downloading...";
            D.body.appendChild(a);
            setTimeout(function() {
                a.click();
                D.body.removeChild(a);
                if (window.URL) {
                    setTimeout(function() {
                        window.URL.revokeObjectURL(a.href);
                    }, 250);
                }
            }, 66);
            $('#submit')[0].textContent = 'Parse & geocode';
            return true;
        } /* end if('download' in a) */


        //do iframe dataURL download (old ch+FF):
        var f = D.createElement("iframe");
        D.body.appendChild(f);
        f.src = "data:" + strMimeType + "," + encodeURIComponent(strData);

        setTimeout(function() {
            D.body.removeChild(f);
        }, 333);
        return true;
    } /* end download() */

    /* Public vars and functions */
    return {
        columns: data_str,
        request_frequency: (frequency / 1000) + ' seconds',
        
    }

    document.addEventListener('DOMContentLoaded', function() {
        toggleModals();
    }, false);    


})();
