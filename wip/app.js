/***************!
    JS Geocoder
    -----------
    Author:     pborbely@fairfaxmedia.com.au
    Born:       August 2014
    Version:    2

    Notes, Dependencies, Credits
    -----------
    Google Maps API Geocoder: https://maps.googleapis.com/maps/api/js
    HTML5 File API: http://dev.w3.org/2009/dap/file-system/file-dir-sys.html
    File API support: https://maps.googleapis.com/maps/api/js
    Download function: http://danml.com/js/download.js

*****************/

var myRawData = [];
var myProcessedData = [];
var data_out = [];
var data_str = 'Addr, Lat, Lng \n'; // first row as header, appended rows as data
var i = 1;
var j = 0;
var file2parse = $('input[type=file]');
var fileName = '';
var latLng = '';


$('#files')[0].addEventListener('change', handleFileSelect, false);

function countResults() {
    j = j + 1;
    // console.log(i,j);

    if (j === i - 1) {
        // console.log('now time to move on');
        $('#counter')[0].textContent = j;
        stringifyProcessedData();
    } else {
        // console.log('there\'s more to do');
        $('#counter')[0].textContent = j;
    }

}

function stringifyProcessedData() {

    for (var k in myProcessedData) {
        // console.log(myProcessedData[k]);

        formAddress = myProcessedData[k].formatted_address;
        // console.log(formAddress);
        formAddress = formAddress.replace(/\,/g, ' | ');
        // console.log(formAddress);
        // data_out.push(formAddress + ',' + myProcessedData[k].geometry.location.A + ',' + myProcessedData[k].geometry.location.k);

        var latLng = myProcessedData[k].geometry.location.toString();
        // console.log(latLng);
        latLng = latLng.replace(/\(/, '');
        latLng = latLng.replace(/\)/, '');
        // latLng = latLng.replace(/\s)/,'');
        latLng = latLng.split(",");
        // console.log(latLng);

        data_out.push(formAddress + ',' + latLng[0] + ',' + latLng[1]);
        // console.log(data_out);
        // console.log(data_out.length);
    }

    for (l in data_out) {
        data_str = data_str + data_out[l] + '\n';
    }

    // console.log(data_str);
    download(data_str, 'geocoded_' + fileName, 'text/plain');
    $('#download').fadeIn();
}


function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    fileName = files[0].name;

    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {
        output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
            f.size, ' bytes, last modified: ',
            f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
            '</li>');
    }
    $('#list').html('<ul>' + output.join('') + '</ul>');
}
// END handleFileSelect



function codeAddress(address, i) {

    var geocoder = new google.maps.Geocoder();
    setTimeout(function() {
        // console.log('geocoder called'),
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                // console.log(results[0]);
                $('#tr_' + i)[0].children[3].textContent = results[0].formatted_address;

                // console.log(results[0].formatted_address.indexOf('Australia') == -1);

                if (results[0].formatted_address.indexOf('Australia') == -1) {
                    $('#tr_' + i)[0].children[3].setAttribute('class', 'nonAus');
                }


                latLng = results[0].geometry.location.toString();
                // console.log(latLng);
                latLng = latLng.replace(/\(/, '');
                latLng = latLng.replace(/\)/, '');
                // latLng = latLng.replace(/\s)/,'');
                latLng = latLng.split(",");
                // console.log(latLng);



                // $('#tr_' + i)[0].children[4].textContent = results[0].geometry.location.A;
                // $('#tr_' + i)[0].children[5].textContent = results[0].geometry.location.k;
                $('#tr_' + i)[0].children[4].textContent = parseFloat(latLng[0]);
                $('#tr_' + i)[0].children[5].textContent = parseFloat(latLng[1]);
                $('#tr_' + i)[0].children[6].textContent = status;
                $('#tr_' + i)[0].children[6].setAttribute('class', 'status_ok');

                myProcessedData.push(results[0]);
                countResults();

            } else {
                // alert("Geocode was not successful for the following reason: " + status);
                $('#tr_' + i)[0].children[6].textContent = status;
                $('#tr_' + i)[0].children[6].setAttribute('class', 'status_err');
                countResults();
            }
            // console.log('geocoder called');

        });
    }, i * 3000);
}
// END codeAddress



function parseData() {

    // var file2parse = $('input[type=file]');
    // $('input[type=file]').parse({
    // var file2parse = $('#files')[0];
    file2parse.parse({
        config: {
            // default config options
            delimiter: "",
            // header field names wil be returned in meta object
            header: true,
            // dynamicTyping converts numeric and boolean data to their respective types
            dynamicTyping: true,
            // preview limits the number of rows to be parsed if > 0
            preview: 0,
            // To stream the input, define a callback function to receive results row-by-row rather than together at the end
            // or undefined
            step: function(results, parser) {
                // console.log(results, parser);
                // console.log(["Row data:", results.data, "Row errors:", results.errors]);
                myRawData.push(results.data[0]);

                // console.log(i, results.data[0]);
                $('#total')[0].textContent = myRawData.length;

                $('#results_table tbody tr:last').after('<tr id="tr_' + i + '"><td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>');

                // console.log(results.meta.fields);
                // GoogleGeocoder(results.data[0].Addr, i);
                codeAddress(results.data[0].Addr, i);

                $('#tr_' + i)[0].children[0].textContent = i;
                $('#tr_' + i)[0].children[2].textContent = results.data[0].Addr;
                i = i + 1;
            },
            // step: undefined,
            encoding: "",
            worker: true,
            comments: false,
            complete: undefined,
            download: false,
            keepEmptyRows: false,
            chunk: undefined
        },
        before: function(file, inputElem, myRawData) {
            // executed before parsing each file begins;
            // what you return here controls the flow
            console.log('executing BEFORE the first parse action');
        },
        error: function(err, file, inputElem, reason) {
            // executed if an error occurs while loading the file,
            // or if before callback aborted for some reason
            console.error(err, file, inputElem, reason);
        },
        complete: function(results, file) {
            // executed after all files are complete
            // console.log("Parsing complete:", results, file);
            // console.log('ALL DONE: ' + results.meta.lines);
            console.log(myRawData.length);
        }
    });

}
// END parseData



function download(strData, strFileName, strMimeType) {
    var D = document,
        a = D.createElement("a");
    strMimeType = strMimeType || "application/octet-stream";

    if (navigator.msSaveBlob) { // IE10+
        return navigator.msSaveBlob(new Blob([strData], {
            type: strMimeType
        }), strFileName);
    } /* end if(navigator.msSaveBlob) */



    if ('download' in a) { //html5 A[download]
        if (window.URL) {
            a.href = window.URL.createObjectURL(new Blob([strData]));

        } else {
            a.href = "data:" + strMimeType + "," + encodeURIComponent(strData);
        }
        a.setAttribute("download", strFileName);
        a.innerHTML = "downloading...";
        D.body.appendChild(a);
        setTimeout(function() {
            a.click();
            D.body.removeChild(a);
            if (window.URL) {
                setTimeout(function() {
                    window.URL.revokeObjectURL(a.href);
                }, 250);
            }
        }, 66);
        return true;
    } /* end if('download' in a) */


    //do iframe dataURL download (old ch+FF):
    var f = D.createElement("iframe");
    D.body.appendChild(f);
    f.src = "data:" + strMimeType + "," + encodeURIComponent(strData);

    setTimeout(function() {
        D.body.removeChild(f);
    }, 333);
    return true;
} /* end download() */
