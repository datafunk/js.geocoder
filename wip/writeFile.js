// http://www.html5rocks.com/en/tutorials/file/filesystem/#toc-requesting-quota
// Note: The file system has been prefixed as of Google Chrome 12:
window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;

// var type = window.TEMPORARY;
var type = window.PERSISTENT;
// var type = PERSISTENT;
var size = 5 * 1024 * 1024; // 5MB



function successCallback(fs) {
    // console.log(['success', fs, size]);
    // console.log('Opened file system: ' + type.name);

    // console.log(fs.root.getFile);
    console.log(fs);

    // fs.root.getFile('log.txt', {
    //     create: true,
    //     exclusive: true
    // }, function(fileEntry) {

    //     var my_file = fileEntry.toURL();
    //     console.log(my_file);        

    //     fileEntry.isFile === true
    //     fileEntry.name == 'log.txt'
    //     fileEntry.fullPath == './log.txt'

    // }, errorHandler);

    /*********************************/

    // fs.root.getFile('log.txt', {
    //     create: true
    // }, function(fileEntry) {

    //     // Create a FileWriter object for our FileEntry (log.txt).
    //     fileEntry.createWriter(function(fileWriter) {

    //         fileWriter.onwriteend = function(e) {
    //             console.log('Write completed.');
    //         };

    //         fileWriter.onerror = function(e) {
    //             console.log('Write failed: ' + e.toString());
    //         };

    //         // Create a new Blob and write it to log.txt.
    //         var blob = new Blob(['Lorem Ipsum'], {
    //             type: 'text/plain'
    //         });

    //         fileWriter.write(blob);

    //     }, errorHandler);

    // }, errorHandler);


}

function errorHandler(e) {
    console.error(e);
    // var msg = '';

    // switch (e.code) {
    //     case FileError.QUOTA_EXCEEDED_ERR:
    //         msg = 'QUOTA_EXCEEDED_ERR';
    //         break;
    //     case FileError.NOT_FOUND_ERR:
    //         msg = 'NOT_FOUND_ERR';
    //         break;
    //     case FileError.SECURITY_ERR:
    //         msg = 'SECURITY_ERR';
    //         break;
    //     case FileError.INVALID_MODIFICATION_ERR:
    //         msg = 'INVALID_MODIFICATION_ERR';
    //         break;
    //     case FileError.INVALID_STATE_ERR:
    //         msg = 'INVALID_STATE_ERR';
    //         break;
    //     default:
    //         msg = 'Unknown Error';
    //         break;
    // };
    // console.error('Error: ' + msg);
}

window.requestFileSystem(type, size, successCallback, errorHandler);


/***************************/

function download(strData, strFileName, strMimeType) {
    var D = document,
        a = D.createElement("a");
        strMimeType= strMimeType || "application/octet-stream";

    if (navigator.msSaveBlob) { // IE10+
        return navigator.msSaveBlob(new Blob([strData], {type: strMimeType}), strFileName);
    } /* end if(navigator.msSaveBlob) */



    if ('download' in a) { //html5 A[download]
        if(window.URL){
            a.href= window.URL.createObjectURL(new Blob([strData]));
            
        }else{
            a.href = "data:" + strMimeType + "," + encodeURIComponent(strData);
        }
        a.setAttribute("download", strFileName);
        a.innerHTML = "downloading...";
        D.body.appendChild(a);
        setTimeout(function() {
            a.click();
            D.body.removeChild(a);
            if(window.URL){setTimeout(function(){ window.URL.revokeObjectURL(a.href);}, 250 );}
        }, 66);
        return true;
    } /* end if('download' in a) */

    
    //do iframe dataURL download (old ch+FF):
    var f = D.createElement("iframe");
    D.body.appendChild(f);
    f.src = "data:" +  strMimeType   + "," + encodeURIComponent(strData);

    setTimeout(function() {
        D.body.removeChild(f);
    }, 333);
    return true;
} /* end download() */




