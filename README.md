# js.Geocoder

This pure JavaScript driven geocoder was born out of frustration and necessity to help my journalist colleagues' tackle data-preparation tasks.

There are geocoders out there aplenty, but services come and go and when one needs to get a job done quickly, there's nothing more frustrating, than coming across a 404 error page. To top that, I found that a lot of services and libraries do a decent job in geocoding datasets, with one - deal-breaking - caveat: they drop the associated data. This means  a tedious merging job between the original and geocoded dataset. This scenario has essentially defined my core requirement set:

- create a geocoder with minimal infrastructure need (to host) and one  that non-technical people can set up with reasonable ease
- retain original data attributes

The former — along with limited option for hosting the application — led me to seek a JavaScript based solution, which meant that one can copy / clone the app, boot up any basic HTTP server on a local machine and get a job done. (The server is required to make AJAX requests to the geocoding service).

## The works

js.geocoder  takes a ```csv``` file with at least an ```ID``` field (to allow easy re-merging) and an ```Addr``` (for Address) field and gives you another ```csv``` file with

- the same ID
- the original address
- the address as interpreted by the geocoding service
- Latitude and Longitude
- Suburb
- Postcode
- the response status (ok, error messages etc)
- ...and it flags if the interpreted address is outside of Australia (I plan to change this to a user defined region later)

## Dependencies

- [HTML5 File API](http://www.w3.org/TR/FileAPI/), see [caniuse.com](http://caniuse.com/#feat=fileapi)
- [jQuery](http://code.jquery.com/jquery-1.11.2.min.js)
- [papaparse.js](http://papaparse.com/)
- third party geocoding service, currently [Google](https://developers.google.com/maps/documentation/geocoding/)

## Roadmap

- user defined client / API key
- Nominatim implementation (option to chose between Google and OSM) as geocoding service
- user defined  region (flag if outside or force region restriction)
- fetch altitude data (3D geocoding FTW!)

## Fine print

As geocoding is reasonably resource intensive (en masse) and relies on massive geo-databases made available by generous third parties, they also imply some — fairly reasonable, I should say — usage terms. To ensure that these terms are understood, I've added a little _read the terms before starting_ enforcement mechanism to the UI. I'm sorry. I know it's annoying, but I also know that most people just wouldn't care... and would get themselves or in worse cases their organisation or some good-soul open host banned from these services.

For there are strict rules to request frequency hitting these geocoding services, I currently imply a 2 second interval between requests. This means that to geocode a few thousand entries may take a while. The good news is: you can just let your browser open and running. It won't time out, thanks to papaparse's awesome, streamed parsing mechanism.
